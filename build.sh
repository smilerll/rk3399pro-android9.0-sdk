#!/bin/bash

RP_BOARD=device/rockchip/rk3399pro/rpdzkj-board.mk
source $RP_BOARD
source build/envsetup.sh >/dev/null && setpaths
#lunch $TARGET_LUNCH
TARGET_PRODUCT=`get_build_var TARGET_PRODUCT`
#set jdk version
export JAVA_HOME=/usr/lib/jvm/java-8-openjdk-amd64
export PATH=$JAVA_HOME/bin:$PATH
export CLASSPATH=.:$JAVA_HOME/lib:$JAVA_HOME/lib/tools.jar
# source environment and chose target product
DEVICE=`get_build_var TARGET_PRODUCT`
BUILD_VARIANT=`get_build_var TARGET_BUILD_VARIANT`
UBOOT_DEFCONFIG=rk3399pro_defconfig
KERNEL_DEFCONFIG=rockchip_defconfig
KERNEL_DTS=`get_build_var TARGET_BUILD_DTB`.img
PACK_TOOL_DIR=RKTools/linux/Linux_Pack_Firmware
IMAGE_PATH=rockdev/Image-$TARGET_PRODUCT
export PROJECT_TOP=`gettop`
#set mkbootimg version
KERNEL_PATH=kernel
KERNEL_DEBUG=`get_build_var TARGET_PREBUILT_KERNEL`
PLATFORM_VERSION=`get_build_var PLATFORM_VERSION`
BOARD_BOOTIMG_HEADER_VERSION=`get_build_var BOARD_BOOTIMG_HEADER_VERSION`
PLATFORM_SECURITY_PATCH=`get_build_var PLATFORM_SECURITY_PATCH`
BOARD_KERNEL_CMDLINE=`get_build_var BOARD_KERNEL_CMDLINE`

echo $TARGET_PRODUCT

function usage()
{
        echo "====USAGE: build.sh modules===="
	echo "BoardConfig_ai3399pro.mk	-build init by BoardConfig_ai3399pro.mk"
        echo "uboot                  	-build uboot"
        echo "kernel                 	-build kernel"
	echo "rd3399pro-android.img  	-build kernel by rd3399pro-android.dts"
        echo "android                	-build android"
        echo "updateimg              	-pack update image"
        echo "ota                    	-pack ab update otapackage image"
        echo "default                	-build all modules"
	exit 1
}

function build_init(){
	echo "============You're building on Android==========="
        echo "Please choose BoardConfig"
        echo "  1. BoardConfig_ai3399pro"
        echo "  2. BoardConfig_rd3399pro"
	echo "  3. BoardConfig_dp3399pro"
        echo -n "Please input num: "
        read board_num

        if [ $board_num -eq 1 ];then
		cp device/rockchip/rk3399pro/BoardConfig-ai3399pro.mk device/rockchip/rk3399pro/rpdzkj-board.mk
		echo "switch to BoardConfig_ai3399pro"
        elif [ $board_num -eq 2 ];then
		cp device/rockchip/rk3399pro/BoardConfig-rd3399pro.mk device/rockchip/rk3399pro/rpdzkj-board.mk
		echo "switch to BoardConfig_rd3399pro"
        elif [ $board_num -eq 3 ];then
		cp device/rockchip/rk3399pro/BoardConfig-dp3399pro.mk device/rockchip/rk3399pro/rpdzkj-board.mk
		echo "switch to BoardConfig_dp3399pro"
	fi
}

function build_check(){
	if [ ! -f "$RP_BOARD" ]; then
		echo "------Please run ./build.sh init , choose BoardConfig-----"
		exit 1
	fi
}

function build_mkimage(){
	# mkimage.sh
	echo "make and copy android images"
	./mkimage.sh
	if [ $? -eq 0 ]; then
		echo "Make image ok!"
	else
		echo "Make image failed!"
		exit 1
	fi
}


function build_uboot(){
	# build uboot
	echo "start build uboot"
	cd u-boot && ./make.sh rk3399pro && cd -
	if [ $? -eq 0 ]; then
		echo "Build uboot ok!"
	else
		echo "Build uboot failed!"
		exit 1
	fi
}

function build_kernel(){
	# build kernel
	echo "Start build kernel"
	cd kernel  && make ARCH=arm64 $KERNEL_DTS -j12 && cd -
	if [ $? -eq 0 ]; then
		echo "Build kernel ok!"
	else
		echo "Build kernel failed!"
		exit 1
	fi

	cp -a $KERNEL_PATH/resource.img $IMAGE_PATH/resource.img
	cp -a $KERNEL_PATH/kernel.img $IMAGE_PATH/kernel.img
	
	mkbootimg --kernel $KERNEL_DEBUG --second kernel/resource.img --os_version $PLATFORM_VERSION --header_version $BOARD_BOOTIMG_HEADER_VERSION --os_patch_level $PLATFORM_SECURITY_PATCH --cmdline "$BOARD_KERNEL_CMDLINE" --output $OUT/boot.img && \
   cp -a $OUT/boot.img $IMAGE_PATH/boot.img
	
	echo "Build boot ok"
}

function build_android(){
	# build android
	echo "start build android"
	source build/envsetup.sh >/dev/null && setpaths
	lunch $TARGET_LUNCH
	make -j12
	build_mkimage
	if [ $? -eq 0 ]; then
		echo "Build android ok!"
	else
		echo "Build android failed!"
		exit 1
	fi
}



function build_otapackage(){
		INTERNAL_OTA_PACKAGE_OBJ_TARGET=obj/PACKAGING/target_files_intermediates/$TARGET_PRODUCT-target_files-*.zip
		INTERNAL_OTA_PACKAGE_TARGET=$TARGET_PRODUCT-ota-*.zip
		echo "generate ota package"
		make otapackage -j4
		./mkimage.sh ota
		cp $OUT/$INTERNAL_OTA_PACKAGE_TARGET $IMAGE_PATH/
		cp $OUT/$INTERNAL_OTA_PACKAGE_OBJ_TARGET $IMAGE_PATH/
}

function build_updateimg(){
#	build_mkimage
	mkdir -p $PACK_TOOL_DIR/rockdev/Image/
	cp -f $IMAGE_PATH/* $PACK_TOOL_DIR/rockdev/Image/

	echo "Make update.img"
	cd $PACK_TOOL_DIR/rockdev && ./mkupdate.sh
	if [ $? -eq 0 ]; then
		echo "Make update image ok!"
	else
		echo "Make update image failed!"
		exit 1
	fi
	cd -
	TIME=`date +%Y%m%d-%H%M%S`
	mv $PACK_TOOL_DIR/rockdev/update.img $IMAGE_PATH/update-rk3399pro-android9.0-$TIME.img
	rm $PACK_TOOL_DIR/rockdev/Image -rf
}


function build_save(){
	PLATFORM_VERSION=`get_build_var PLATFORM_VERSION`
	DATE=$(date  +%Y%m%d.%H%M)
	STUB_PATH=Image/"$KERNEL_DTS"_"$PLATFORM_VERSION"_"$DATE"_RELEASE_TEST
	STUB_PATH="$(echo $STUB_PATH | tr '[:lower:]' '[:upper:]')"
	export STUB_PATH=$PROJECT_TOP/$STUB_PATH
	export STUB_PATCH_PATH=$STUB_PATH/PATCHES

	mkdir -p $STUB_PATH

	#Generate patches
	#.repo/repo/repo forall  -c "$PROJECT_TOP/device/rockchip/common/gen_patches_body.sh"

	#.repo/repo/repo forall  -c '[ "$REPO_REMOTE" = "rk" ] && { REMOTE_DIFF=`git log $REPO_REMOTE/$REPO_RREV..HEAD`; LOCAL_DIFF=`git diff`; [ -n "$REMOTE_DIFF" ] && { mkdir -p $STUB_PATCH_PATH/$REPO_PATH/; git format-patch $REPO_REMOTE/$REPO_RREV..HEAD -o $STUB_PATCH_PATH/$REPO_PATH; git merge-base HEAD $REPO_REMOTE/$REPO_RREV | xargs git show -s > $STUB_PATCH_PATH/$REPO_PATH/git-merge-base.txt; } || :; [ -n "$LOCAL_DIFF" ] && { mkdir -p $STUB_PATCH_PATH/$REPO_PATH/; git reset HEAD ./; git diff > $STUB_PATCH_PATH/$REPO_PATH/local_diff.patch; } || :; }'

	#Copy stubs
	#cp commit_id.xml $STUB_PATH/manifest_${DATE}.xml

	mkdir -p $STUB_PATCH_PATH/kernel
	cp kernel/.config $STUB_PATCH_PATH/kernel
	cp kernel/vmlinux $STUB_PATCH_PATH/kernel

	mkdir -p $STUB_PATH/IMAGES/
	cp $IMAGE_PATH/* $STUB_PATH/IMAGES/
	cp build.sh $STUB_PATH/build.sh
	#Save build command info
	echo "UBOOT:  defconfig: $UBOOT_DEFCONFIG" >> $STUB_PATH/build_cmd_info
	echo "KERNEL: defconfig: $KERNEL_DEFCONFIG, dts: $KERNEL_DTS" >> $STUB_PATH/build_cmd_info
	echo "ANDROID:$DEVICE-$BUILD_VARIANT" >> $STUB_PATH/build_cmd_info
}

function build_all(){
	build_uboot
	build_kernel
	build_android
	build_updateimg
}

function build_ota(){
	build_uboot
	build_kernel
	build_android
	build_otapackage
	build_updateimg
}


if echo $@|grep -wqE "help|-h"; then
	usage
	exit 0
fi

OPTIONS="$@"
for option in ${OPTIONS:-all}; do
	echo "processing option: $option"
	case $option in
		BoardConfig_ai3399pro.mk)
			cp device/rockchip/rk3399pro/BoardConfig-ai3399pro.mk device/rockchip/rk3399pro/rpdzkj-board.mk
			echo "switch to BoardConfig_ai3399pro"
			;;
		BoardConfig_rd3399pro.mk)
			cp device/rockchip/rk3399pro/BoardConfig-rd3399pro.mk device/rockchip/rk3399pro/rpdzkj-board.mk
			echo "switch to BoardConfig_rd3399pro"
			;;			
		updateimg)
                        build_updateimg
                        ;;
		*img)
			KERNEL_DTS=$option
			build_kernel			
			;;
		init)
			build_init
			;;
		*)
			build_check
			eval build_$option || usage
			;;
	esac
done




