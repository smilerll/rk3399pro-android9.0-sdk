#include <linux/module.h>
#include <linux/err.h>
#include <linux/gpio.h>
#include <linux/kernel.h>
#include <linux/of_gpio.h>
#include <linux/of_platform.h>
#include <linux/delay.h>
#include <linux/wakelock.h>
#include <linux/of.h>
#include <linux/of_fdt.h>

struct gpio_gpio {
	int gpio_num;
	int val;
};


struct power_en_gpio {
	struct gpio_gpio gpio_5ven;
	struct gpio_gpio gpio_3v3_en;
	struct gpio_gpio gpio_1v8_en;
	struct gpio_gpio usb_5v_en;
	struct gpio_gpio usb_en;
	struct gpio_gpio uart5_en;
	struct gpio_gpio hub_rst;
	struct gpio_gpio gpio_4g_en;
	struct gpio_gpio wifi_power_en;
	struct gpio_gpio fan_en;
	int sleep_flag;
	struct wake_lock rp_wake_lock;
};


static int power_en_probe(struct platform_device *pdev)
{
        int ret = 0;
        struct device_node *np = pdev->dev.of_node;
	struct power_en_gpio *data;

	printk("----------------test----------------");

	data = devm_kzalloc(&pdev->dev, sizeof(struct power_en_gpio),GFP_KERNEL);
	if (!data) {
                dev_err(&pdev->dev, "failed to allocate memory\n");
                return -ENOMEM;
        }
	memset(data, 0, sizeof(struct power_en_gpio));

        data->gpio_5ven.gpio_num = of_get_named_gpio_flags(np, "5v_power_en", 0, NULL);
        if (!gpio_is_valid(data->gpio_5ven.gpio_num))
                data->gpio_5ven.gpio_num = -1;

        data->gpio_3v3_en.gpio_num = of_get_named_gpio_flags(np, "3v3_power_en", 0, NULL);
        if (!gpio_is_valid(data->gpio_3v3_en.gpio_num))
                data->gpio_3v3_en.gpio_num = -1;
				
        data->gpio_1v8_en.gpio_num = of_get_named_gpio_flags(np, "1v8_power_en", 0, NULL);
        if (!gpio_is_valid(data->gpio_1v8_en.gpio_num))
                data->gpio_1v8_en.gpio_num = -1;

        data->usb_5v_en.gpio_num = of_get_named_gpio_flags(np, "usb_5v_en", 0, NULL);
        if (!gpio_is_valid(data->usb_5v_en.gpio_num))
                data->usb_5v_en.gpio_num = -1;
                
        data->usb_en.gpio_num = of_get_named_gpio_flags(np, "usb_en", 0, NULL);
        if (!gpio_is_valid(data->usb_en.gpio_num))
                data->usb_en.gpio_num = -1;

        data->uart5_en.gpio_num = of_get_named_gpio_flags(np, "uart5_en", 0, NULL);
        if (!gpio_is_valid(data->uart5_en.gpio_num))
                data->uart5_en.gpio_num = -1;
                
        data->hub_rst.gpio_num = of_get_named_gpio_flags(np, "hub_rst", 0, NULL);
        if (!gpio_is_valid(data->hub_rst.gpio_num))
                data->hub_rst.gpio_num = -1;
				
        data->gpio_4g_en.gpio_num = of_get_named_gpio_flags(np, "4g_power_en", 0, NULL);
        if (!gpio_is_valid(data->gpio_4g_en.gpio_num))
                data->gpio_4g_en.gpio_num = -1;

        data->wifi_power_en.gpio_num = of_get_named_gpio_flags(np, "wifi_power_en", 0, NULL);
        if (!gpio_is_valid(data->wifi_power_en.gpio_num))
                data->wifi_power_en.gpio_num = -1;

        data->fan_en.gpio_num = of_get_named_gpio_flags(np, "fan_open", 0, NULL);
        if (!gpio_is_valid(data->fan_en.gpio_num))
                data->fan_en.gpio_num = -1;
	of_property_read_u32(np, "rp_not_deep_leep", &data->sleep_flag);


	platform_set_drvdata(pdev, data);
	

	if(data->gpio_5ven.gpio_num != -1){
		ret = gpio_request(data->gpio_5ven.gpio_num, "power_5v_en");
        	if (ret < 0){
			printk("data->gpio_5ven request error\n");
//        	        return ret;
		}else{
			gpio_direction_output(data->gpio_5ven.gpio_num, 1);
        		gpio_set_value(data->gpio_5ven.gpio_num, 1);
		}
	}

	if(data->gpio_3v3_en.gpio_num != -1){
		ret = gpio_request(data->gpio_3v3_en.gpio_num, "gpio_3v3_en");
        	if (ret < 0){
			printk("data->gpio_3v3_en request error\n");
		}else{
			gpio_direction_output(data->gpio_3v3_en.gpio_num, 1);
        		gpio_set_value(data->gpio_3v3_en.gpio_num, 1);
		}
	}

	if(data->gpio_1v8_en.gpio_num != -1){
		ret = gpio_request(data->gpio_1v8_en.gpio_num, "gpio_1v8_en");
        	if (ret < 0){
			printk("data->gpio_1v8_en request error\n");
		}else{
			gpio_direction_output(data->gpio_1v8_en.gpio_num, 1);
        		gpio_set_value(data->gpio_1v8_en.gpio_num, 1);
		}
	}

	if(data->usb_5v_en.gpio_num != -1){
		ret = gpio_request(data->usb_5v_en.gpio_num, "usb_5v_en");
        	if (ret < 0){
			printk("data->usb_5v_en request error\n");
		}else{
			gpio_direction_output(data->usb_5v_en.gpio_num, 1);
        		gpio_set_value(data->usb_5v_en.gpio_num, 1);
		}
	}
        
	if(data->usb_en.gpio_num != -1){
		ret = gpio_request(data->usb_en.gpio_num, "usb_en");
        	if (ret < 0){
			printk("data->usb_en request error\n");
		}else{
			gpio_direction_output(data->usb_en.gpio_num, 1);
  			gpio_set_value(data->usb_en.gpio_num, 1);
		}
	}
  
	if(data->uart5_en.gpio_num != -1){
		ret = gpio_request(data->uart5_en.gpio_num, "uart5_en");
        	if (ret < 0){
			printk("data->uart5_en request error\n");
		}else{
			gpio_direction_output(data->uart5_en.gpio_num, 1);
  			gpio_set_value(data->uart5_en.gpio_num, 1);
		}
	}
	
	if(data->hub_rst.gpio_num != -1){
		ret = gpio_request(data->hub_rst.gpio_num, "hub_rst");
        	if (ret < 0){
			printk("data->hub_rst request error\n");
		}else{
//			gpio_direction_output(data->hub_rst.gpio_num, 0);
//        		gpio_set_value(data->hub_rst.gpio_num, 0);
//			msleep(100);
			gpio_direction_output(data->hub_rst.gpio_num, 1);
        		gpio_set_value(data->hub_rst.gpio_num, 1);
		}
	}

	if(data->gpio_4g_en.gpio_num != -1){
		ret = gpio_request(data->gpio_4g_en.gpio_num, "gpio_4g_en");
        	if (ret < 0){
			printk("data->gpio_4g_en request error\n");
		}else{
			gpio_direction_output(data->gpio_4g_en.gpio_num, 1);
        		gpio_set_value(data->gpio_4g_en.gpio_num, 1);
		}
	}
        
	if(data->wifi_power_en.gpio_num != -1){
		ret = gpio_request(data->wifi_power_en.gpio_num, "wifi_power_en");
        	if (ret < 0){
			printk("data->wifi_power_en request error\n");
		}else{
			gpio_direction_output(data->wifi_power_en.gpio_num, 1);
  			gpio_set_value(data->wifi_power_en.gpio_num, 1);
		}
	}
  
	if(data->fan_en.gpio_num != -1){
		ret = gpio_request(data->fan_en.gpio_num, "fan_en");
        	if (ret < 0){
			printk("data->fan_en request error\n");
		}else{
			gpio_direction_output(data->fan_en.gpio_num, 1);
  			gpio_set_value(data->fan_en.gpio_num, 1);
		}
	}
	
	if(data->sleep_flag != 0){
		wake_lock_init(&data->rp_wake_lock,WAKE_LOCK_SUSPEND, "rpdzkj_no_deep_sleep");
		wake_lock(&data->rp_wake_lock);
	}

        return 0;
};

static int power_en_remove(struct platform_device *pdev)
{
        struct power_en_gpio *data = platform_get_drvdata(pdev);


	if(data->gpio_5ven.gpio_num != -1){
		gpio_direction_output(data->gpio_5ven.gpio_num, 0);
		gpio_free(data->gpio_5ven.gpio_num);
	}
	if(data->gpio_3v3_en.gpio_num != -1){
		gpio_direction_output(data->gpio_3v3_en.gpio_num, 0);
		gpio_free(data->gpio_3v3_en.gpio_num);
	}
	if(data->gpio_1v8_en.gpio_num != -1){
		gpio_direction_output(data->gpio_1v8_en.gpio_num, 0);
		gpio_free(data->gpio_1v8_en.gpio_num);
	}
	if(data->usb_5v_en.gpio_num != -1){
		gpio_direction_output(data->usb_5v_en.gpio_num, 0);
		gpio_free(data->usb_5v_en.gpio_num);
	}
	if(data->usb_en.gpio_num != -1){
		gpio_direction_output(data->usb_en.gpio_num, 0);
		gpio_free(data->usb_en.gpio_num);
	}
	if(data->uart5_en.gpio_num != -1){
		gpio_direction_output(data->uart5_en.gpio_num, 0);
		gpio_free(data->uart5_en.gpio_num);
	}
	if(data->hub_rst.gpio_num != -1){
		gpio_direction_output(data->hub_rst.gpio_num, 0);
		gpio_free(data->hub_rst.gpio_num);
	}
	if(data->gpio_4g_en.gpio_num != -1){
		gpio_direction_output(data->gpio_4g_en.gpio_num, 0);
		gpio_free(data->gpio_4g_en.gpio_num);
	}
	if(data->wifi_power_en.gpio_num != -1){
		gpio_direction_output(data->wifi_power_en.gpio_num, 0);
		gpio_free(data->wifi_power_en.gpio_num);
	}
	if(data->fan_en.gpio_num != -1){
		gpio_direction_output(data->fan_en.gpio_num, 0);
		gpio_free(data->fan_en.gpio_num);
	}
	if(data->sleep_flag != 0){
		wake_unlock(&data->rp_wake_lock);
	}

        return 0;
}

#ifdef CONFIG_PM 
static int power_en_suspend(struct device *dev) 
{ 
	struct platform_device *pdev = to_platform_device(dev);
        struct power_en_gpio *data = platform_get_drvdata(pdev);
 
	if(data->gpio_5ven.gpio_num != -1){
		gpio_direction_output(data->gpio_5ven.gpio_num, 0);
		gpio_free(data->gpio_5ven.gpio_num);
	}
	if(data->gpio_3v3_en.gpio_num != -1){
		gpio_direction_output(data->gpio_3v3_en.gpio_num, 0);
		gpio_free(data->gpio_3v3_en.gpio_num);
	}
	if(data->gpio_1v8_en.gpio_num != -1){
		gpio_direction_output(data->gpio_1v8_en.gpio_num, 0);
		gpio_free(data->gpio_1v8_en.gpio_num);
	}
	if(data->usb_5v_en.gpio_num != -1){
		gpio_direction_output(data->usb_5v_en.gpio_num, 0);
		gpio_free(data->usb_5v_en.gpio_num);
	}
	if(data->usb_en.gpio_num != -1){
		gpio_direction_output(data->usb_en.gpio_num, 0);
		gpio_free(data->usb_en.gpio_num);
	}
	if(data->uart5_en.gpio_num != -1){
		gpio_direction_output(data->uart5_en.gpio_num, 0);
		gpio_free(data->uart5_en.gpio_num);
	}
	if(data->hub_rst.gpio_num != -1){
		gpio_direction_output(data->hub_rst.gpio_num, 0);
		gpio_free(data->hub_rst.gpio_num);
	}
	if(data->gpio_4g_en.gpio_num != -1){
		gpio_direction_output(data->gpio_4g_en.gpio_num, 0);
		gpio_free(data->gpio_4g_en.gpio_num);
	}
	if(data->wifi_power_en.gpio_num != -1){
		gpio_direction_output(data->wifi_power_en.gpio_num, 0);
		gpio_free(data->wifi_power_en.gpio_num);
	}
	if(data->fan_en.gpio_num != -1){
		gpio_direction_output(data->fan_en.gpio_num, 0);
		gpio_free(data->fan_en.gpio_num);
	}
	if(data->sleep_flag != 0){
		wake_unlock(&data->rp_wake_lock);
	}
 
        return 0; 
} 
 
static int power_en_resume(struct device *dev) 
{ 
	struct platform_device *pdev = to_platform_device(dev);
        struct power_en_gpio *data = platform_get_drvdata(pdev);
 
	if(data->gpio_5ven.gpio_num != -1){
		gpio_direction_output(data->gpio_5ven.gpio_num, 1);
		gpio_free(data->gpio_5ven.gpio_num);
	}
	if(data->gpio_3v3_en.gpio_num != -1){
		gpio_direction_output(data->gpio_3v3_en.gpio_num, 1);
		gpio_free(data->gpio_3v3_en.gpio_num);
	}
	if(data->gpio_1v8_en.gpio_num != -1){
		gpio_direction_output(data->gpio_1v8_en.gpio_num, 1);
		gpio_free(data->gpio_1v8_en.gpio_num);
	}
	if(data->usb_5v_en.gpio_num != -1){
		gpio_direction_output(data->usb_5v_en.gpio_num, 1);
		gpio_free(data->usb_5v_en.gpio_num);
	}
	if(data->usb_en.gpio_num != -1){
		gpio_direction_output(data->usb_en.gpio_num, 1);
		gpio_free(data->usb_en.gpio_num);
	}
	if(data->uart5_en.gpio_num != -1){
		gpio_direction_output(data->uart5_en.gpio_num, 1);
		gpio_free(data->uart5_en.gpio_num);
	}
	if(data->hub_rst.gpio_num != -1){
		gpio_direction_output(data->hub_rst.gpio_num, 1);
		gpio_free(data->hub_rst.gpio_num);
	}
	if(data->gpio_4g_en.gpio_num != -1){
		gpio_direction_output(data->gpio_4g_en.gpio_num, 1);
		gpio_free(data->gpio_4g_en.gpio_num);
	}
	if(data->wifi_power_en.gpio_num != -1){
		gpio_direction_output(data->wifi_power_en.gpio_num, 1);
		gpio_free(data->wifi_power_en.gpio_num);
	}
	if(data->fan_en.gpio_num != -1){
		gpio_direction_output(data->fan_en.gpio_num, 1);
		gpio_free(data->fan_en.gpio_num);
	}
	if(data->sleep_flag != 0){
		wake_unlock(&data->rp_wake_lock);
	}
        return 0; 
} 
 
static const struct dev_pm_ops power_en_pm_ops = { 
        .suspend        = power_en_suspend, 
        .resume         = power_en_resume, 
}; 
#endif

static const struct of_device_id power_en_of_match[] = {
        { .compatible = "5v-en-gpio" },
        { }
};

static struct platform_driver power_en_driver = {
        .probe = power_en_probe,
        .remove = power_en_remove,
        .driver = {
                .name           = "5v-en-gpio",
                .of_match_table = of_match_ptr(power_en_of_match),
#ifdef CONFIG_PM
                .pm     = &power_en_pm_ops,
#endif

        },
};

module_platform_driver(power_en_driver);

MODULE_LICENSE("GPL");
