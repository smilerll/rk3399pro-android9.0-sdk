#ifndef SDBP_H
#define SDBP_H

#include "serial.h"

typedef struct UARTPara
{
    byte com;
    byte baudrate;
    byte databit;
    byte stopbit;
    byte checkbit;
}UARTPara;

/*
 *
 * sdbp const def
 *
 */
#define FRM_MAX_LEN 960;
#define FRM_DATA_MAX_LEN 952;

typedef struct
{
  byte type;
  byte funcode;
}ID;

ID ackID;
ID nackID;
ID enterID;
ID confirmID;
ID eraseID;
ID writeID;
ID rebootID;
ID uartID;
ID verID;


byte bufWrite[960];

//byte CheckImgFile(const byte *filedatas, int startpoint, int filelen);
byte CheckImgData1(const byte *filedatas, int startpoint, int filelen);
byte CheckImgData2(const byte *filedatas, int startpoint, int filelen);

int GetFirstSyncFrame(byte* bufRead, int len, int* head, int* tail);
int SDBP_PUB_ACK(byte* bufRead, ID id,int idx);
int SDBP_PUB_NACK(byte* bufRead, ID id,int idx);
int SDBP_NACK(byte* bufRead, int idx);
void SDBP_UPD_ENTER(void);
void SDBP_UPD_CONFIRM_I(void);
int SDBP_UPD_CONFIRM_O(byte* bufRead, int idx);


void SDBP_CANCEL_PROTECT_I_C1();
void SDBP_CANCEL_PROTECT_I_C2();
void SDBP_CANCEL_PROTECT_I_C3();
void SDBP_CANCEL_PROTECT_I_C4();
void SDBP_CANCEL_PROTECT_I_C5();
void SDBP_CANCEL_PROTECT_I_C6();
void SDBP_CANCEL_PROTECT_I_C7();
void SDBP_CANCEL_PROTECT_I_C8();

void SDBP_UPD_ERASE_I1(void);
void SDBP_UPD_ERASE_I2(int num);
int SDBP_UPD_ERASE_O1(byte* bufRead, int idx);
int SDBP_UPD_ERASE_O2(byte* bufRead, int idx, int num);
int SDBP_UPD_WRITE_I(byte* datas,int offset,int datalen);
int SDBP_UPD_WRITE_O(byte* bufRead, int idx);
void SDBP_CFG_UART_I(UARTPara uartpara,byte autosave);
void SDBP_UPD_REBOOT();
void SDBP_DBG_FLASH_I(int offset,int num);
int SDBP_DBG_FLASH_O(byte* bufRead, int idx,byte* datas,int offset, int len);
void SDBP_QUE_VER_I(void);
int SDBP_QUE_VER_O(byte* bufRead, int idx);
int FletCher16_sdbp(byte* buffer, int offset, int count);

#define IMGFILE_HEADREAR_LEN    4

unsigned short CalcCRC16(const char *datas, int startpoint, int len);


#endif

