#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <termios.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <pthread.h>
#include "agps.h"
#include "tdgnss.h"

int line_len(char *p);

#define P_CH_OK(x)    ((x) && ((x) < buff_end))

TD_INFO td_agps_info;

int is_letter(char ch)
{
    if(((ch >= 'a') && (ch <= 'z')) || ((ch >= 'A') && (ch <= 'Z')))
        return 1;

    return 0;
}

int is_num(char ch)
{
    if((ch >= '0') && (ch <= '9'))
        return 1;

    return 0;
}

char *supl_get_first_head(char *head, char *buff)
{
    if(!head || !buff)
        return 0;

    return strstr(buff, head);
}

int copy_line(char *dst, char *src)
{
    int i = 0;

    while(1)
    {
        if((src[i] == '\r') && (src[i + 1] == '\n'))
        {
            dst[i] = src[i];
            dst[i + 1] = src[i + 1];
            dst[i + 2] = 0;
            return i + 2;
        }
        else
            dst[i] = src[i];

        if(src[i] == 0)
        {
            return i;
        }

        i++;
    }

    return 0;
}

char *supl_get_next_num(char *buff)
{
    int i = 0, find_no_num = 0;

    if(!buff)
        return 0;

    while(buff[i])
    {
        if((buff[i] == '.') || (buff[i] == '-') || is_num(buff[i]))    //
        {
            if(find_no_num)
            {
                return (buff + i);
            }
        }
        else
        {
            find_no_num = 1;
        }

        i++;
    }

    return 0;
}
/*
#ifdef DEBUG_SUPL
void print_position_response()
{
    D("T %ld %ld %ld %ld\n", td_agps_info.pos_time.gps_week, td_agps_info.pos_time.gps_tow,
      td_agps_info.pos_time.stamp.tv_sec, td_agps_info.pos_time.stamp.tv_usec);

    D("L %f %f %d\n", td_agps_info.pos.lat, td_agps_info.pos.lon, td_agps_info.pos.uncertainty);
}


void print_ephemeris_response()
{
    int i;

    for(i = 0; i < td_agps_info.cnt_eph; i++)
    {
        D("e %d %d %d %d %d %d %d %d %d %d",
          td_agps_info.gps_ephemeris[i].prn,
          td_agps_info.gps_ephemeris[i].delta_n,
          td_agps_info.gps_ephemeris[i].M0,
          td_agps_info.gps_ephemeris[i].A_sqrt,
          td_agps_info.gps_ephemeris[i].OMEGA_0,
          td_agps_info.gps_ephemeris[i].i0,
          td_agps_info.gps_ephemeris[i].w,
          td_agps_info.gps_ephemeris[i].OMEGA_dot,
          td_agps_info.gps_ephemeris[i].i_dot,
          td_agps_info.gps_ephemeris[i].e);
        D(" %d %d %d %d %d %d",
          td_agps_info.gps_ephemeris[i].Cuc,
          td_agps_info.gps_ephemeris[i].Cus,
          td_agps_info.gps_ephemeris[i].Crc,
          td_agps_info.gps_ephemeris[i].Crs,
          td_agps_info.gps_ephemeris[i].Cic,
          td_agps_info.gps_ephemeris[i].Cis);
        D(" %d %d %d %d %d %d",
          td_agps_info.gps_ephemeris[i].toe,
          td_agps_info.gps_ephemeris[i].IODC,
          td_agps_info.gps_ephemeris[i].toc,
          td_agps_info.gps_ephemeris[i].AF0,
          td_agps_info.gps_ephemeris[i].AF1,
          td_agps_info.gps_ephemeris[i].AF2);
        D(" %d %d %d %d %d\n",
          td_agps_info.gps_ephemeris[i].bits,
          td_agps_info.gps_ephemeris[i].ura,
          td_agps_info.gps_ephemeris[i].health,
          td_agps_info.gps_ephemeris[i].tgd,
          td_agps_info.gps_ephemeris[i].AODA);
    }
}
#endif
*/

int line_len(char *p)
{
    int i = 0;

    while(1)
    {
        if((p[i] == '\r') && (p[i + 1] == '\n'))
        {

            return i + 2;
        }

        if(p[i] == 0)
            return i;

        i++;
    }
}


//handle_supl_client_response
int handle_supl_client_response(char *buff, int size)
{
    int i = 0, len, ret;
    char *p_ch = buff;
    char *buff_end = buff + size;

    if(!buff)
        return -1;

    for(i = 0; i < 32; i++)
    {
        p_ch = supl_get_first_head("$CCGEP", p_ch);

        if(!P_CH_OK(p_ch))
            break;

        if(line_len(p_ch) >= 256)
        {
          return -2;
        }

        char *p_num = supl_get_next_num(p_ch);

        if(p_num == 0) //supl-client error!!!
        {          
          return -3;
        }

        if(atol(p_num) != (i + 1))
            continue;
        else
            td_agps_info.agps_data.ehp_new.eph_mask |= (0x1 << i);

        len = copy_line(td_agps_info.agps_data.ehp_new.line[i], p_ch);

        if(len == 0)
        {          
          return -4;
        }

        p_ch += len;
    }


    //if(td_agps_info.agps_data.ehp_new.eph_mask == 0 || td_agps_info.agps_data.time_loc_new.mask == 0) //get no any eph or get no time_loc
    if(td_agps_info.agps_data.ehp_new.eph_mask == 0) //get no any eph
        ret = -5;
    else
        ret = 0;

    return ret;
}


