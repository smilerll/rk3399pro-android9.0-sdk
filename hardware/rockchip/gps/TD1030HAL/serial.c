#include "serial.h"
#include "tdgnss.h"

extern int com_fd;

void BaudRateChange(int baud)
{
  if(-1 == com_fd)
    return;
  
  struct termios tio; 
  tcgetattr(com_fd, &tio);

  cfsetispeed(&tio, baud);
  cfsetospeed(&tio, baud);

  tcsetattr(com_fd, TCSANOW, &tio);

}
void DiscardBuffer(void)
{
  if(-1 == com_fd)
    return;

  tcflush(com_fd,TCIOFLUSH);
}

int TxData(unsigned char *buffer, int length)
{
  int tx_len = 0;
  if(-1 == com_fd)
    return 0;

  do
  {
    tx_len = write( com_fd, buffer, length );
  }
  while (tx_len < 0 && (errno == EINTR || errno == EAGAIN));

  return tx_len;
  
}

int RxData(unsigned char *buffer, int length)
{
  if(-1 == com_fd)
    return 0;

  return read(com_fd, buffer, length);
}

void Write55(void)
{
  byte buf[2] = {0x55, 0x55};

  if(com_fd != -1)
  {
    write(com_fd, buf, 2);
  }
}

